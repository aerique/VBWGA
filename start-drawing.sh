#!/bin/sh

PNG=
TARGET_FITNESS="1e-9"

if [ "${1}" = "" ]; then
    echo "usage: start-drawing.sh file.png"
    exit 0
else
    PNG=${1}
fi

if [ "${2}" != "" ]; then
    TARGET_FITNESS=${2}
fi

sbcl --dynamic-space-size 4000 --load victory-boogie-woogie-genetic-algorithm.lisp --eval "(progn (vbw::write-pdf (vbw::resolution-independent-drawing (vbw::main \"${PNG}\" :target-fitness ${TARGET_FITNESS})) \"vbw.pdf\") (quit))"
